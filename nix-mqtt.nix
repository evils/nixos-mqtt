{ config, lib, pkgs, ...}:

# TODO: find a better name and description

let
  commit = "8bdb8b2be79eae65244358e4ce231fa4268d0207";
  hash = "sha256-1ZxhlKXLkdhs9/aazljC8wtAmhylTd28blGF2YudGJQ=";
  nix-mqtt = pkgs.fetchurl {
    inherit hash;
    url = "https://gitlab.com/evils/nix-mqtt/-/raw/${commit}/nixos-channel-age.sh";
    executable = true;
  };
in
{
  users = {
    groups.nix-mqtt.members = [ "nix-mqtt" ];
    users.nix-mqtt = {
      description = "nix channel mqtt publisher";
      group = "nix-mqtt";
      isSystemUser = true;
    };
  };

  systemd = {
    services.nix-mqtt = {
      description = "nix channel mqtt publisher";
      after = [ "network.target" "mosquitto.service" ];
      wantedBy = [ "multi-user.target" ];
      environment.NIX_PATH = "nixpkgs=${pkgs.path}";
      path = [ pkgs.nix ];
      serviceConfig = {
        Type = "oneshot";
        User = "nix-mqtt";
        ExecStart = "${nix-mqtt} --host localhost --expire 2000";
      };
    };

    timers.nix-mqtt = {
      description = "nix channel mqtt publishing timer";
      wantedBy = [ "nix-mqtt.service" "timers.target" ];
      partOf = [ "nix-mqtt.service" ];
      timerConfig = {
        Unit = "nix-mqtt.service";
        OnUnitInactiveSec = "420";
        RandomizedDelaySec = "59s";
      };
    };
  };

}
