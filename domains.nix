{ config, lib, pkgs, ...}: {


  services.cfdyndns = {
    enable = true;
    apikeyFile = "${./cfkey}";
    email = "evils.devils@protonmail.com";
    records = [
      "mqtt.evils.eu"
      "nix.evils.eu"
      "mqtt.nix.evils.eu"
      "nix.mqtt.evils.eu"
    ];
  };

  security.acme = {
    acceptTerms = true;
    email = "evils.devils@protonmail.com";
    certs = {
      "mqtt.evils.eu" = {
        dnsProvider = "cloudflare";
        credentialsFile = "${./acme-cfcred}";
        extraDomainNames = [
          "nix.evils.eu"
          "mqtt.nix.evils.eu"
          "nix.mqtt.evils.eu"
        ];
      };
    };
  };

}
