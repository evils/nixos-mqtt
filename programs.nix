{ config, lib, pkgs, ... }: {

  environment.systemPackages = with pkgs; [
    # system utilities
    bc jq gcc git zsh tree
    rsync unzip gnupg
    usbutils moreutils lshw lsof
    wget dhcp
    # informational
    arp-scan htop smartmontools pv
    # miscellaneous
    vim neofetch speedtest-cli stress thefuck
    direnv alacritty
  ];

  programs = {
    vim.defaultEditor = true;
    gnupg.agent.enable = true;

    zsh = {
      enable = true;
      autosuggestions = {
        enable = true;
        strategy = "match_prev_cmd";
      };
      enableCompletion = true;
      syntaxHighlighting.enable = true;
      interactiveShellInit = ''
        ZSH_DISABLE_COMPFIX=true
        export ZSH=$HOME/.oh-my-zsh
        source $ZSH/oh-my-zsh.sh
        ZSH_CUSTOM=$ZSH/custom/
        export NIX_PAGER=cat
        export DIRENV_LOG_FORMAT=""
        eval "$(direnv hook zsh)"
      '';
      promptInit = "";
    };
  };

}
