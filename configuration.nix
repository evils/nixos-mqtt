# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports = [ 
    ./hardware-configuration.nix

    ./users.nix
    ./programs.nix
    ./i18n.nix

    ./domains.nix
    ./mqtt.nix
    ./nix-mqtt.nix
  ];

  # save some time on rebuilds...
  documentation.enable = false;
  documentation.man.enable = false;

  system.autoUpgrade = {
    enable = true;
    allowReboot = true;
    randomizedDelaySec = "42s";
  };

  nix = {
    autoOptimiseStore = true;
    gc.automatic = true;
  };

  boot.loader.grub.devices = [ "/dev/sda" ];

  #hardware.rasdaemon.enable = true;

  networking = {
    hostName = "nixos-mqtt";
    useDHCP = false; # true is deprecated but still the default, set per interface
    interfaces.eth0.useDHCP = true;
    usePredictableInterfaceNames = true;
  };

  services = {
    openssh = {
      enable = true;
      permitRootLogin = "prohibit-password";
    };

    tuptime.enable = true;
    #clarissa.enable = true;
  };

  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBj6N5zlcXIg5RzmTFovzGU3a80LvXUmnkBbIT29HuoS evils@valix"
  ];


  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "21.05"; # Did you read the comment?

}

