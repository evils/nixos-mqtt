{ config, lib, pkgs, ... }: {

  users.users.evils = {
    isNormalUser = true;
    shell = pkgs.zsh;
    extraGroups = [ "wheel" "dialout" "input" "beep" ];
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBj6N5zlcXIg5RzmTFovzGU3a80LvXUmnkBbIT29HuoS evils@valix"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHYF9FEL4Unu8i8hXHgwG40wjhZs8C/uNqqHIr7+087k evils@jamshed"
    ];
  };

}
