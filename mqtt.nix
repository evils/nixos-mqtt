{ config, lib, pkgs, ...}: {

  services.mosquitto = {
    enable = true;
    listeners = [ {
      port = 1883;
      address = "nix.evils.eu";
      acl = [ "topic read nix/#" ];
      settings.allow_anonymous = true;
    }
    {
      port = 1883;
      address = "localhost";
      acl = [ "topic readwrite #" "topic read $SYS/#" ];
      settings.allow_anonymous = true;
    }
    {
      port = 8883;
      acl = [ "topic read nix/#" ];
      settings = {
        allow_anonymous = true;
        cafile = "${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt";
        certfile = "${config.security.acme.certs."mqtt.evils.eu".directory}/fullchain.pem";
        keyfile = "${config.security.acme.certs."mqtt.evils.eu".directory}/key.pem";
      };
      users.evils = {
        acl = [ "readwrite #" "read $SYS/#" ];
        hashedPasswordFile = "${./mqtt-evils-pwd}";
      };
    } ];
  };

  users.users.mosquitto.extraGroups = [ "acme" ];

  networking = {
    firewall = {
      allowedTCPPorts = [
        1883
        8883
      ];
    };
  };

  environment.systemPackages = with pkgs; [
    mosquitto
  ];

}
